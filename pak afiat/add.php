<?php
// Mendapatkan koneksi ke database
$host = 'localhost';  // Ganti dengan host database Anda
$dbname = 'tutorial';  // Ganti dengan nama database Anda
$username = 'root';  // Ganti dengan nama pengguna database Anda
$password = '';  // Ganti dengan kata sandi database Anda

try {
    $dbh = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Koneksi ke database gagal: " . $e->getMessage());
}

// Memeriksa apakah ada permintaan POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Mendapatkan data yang dikirimkan melalui POST
    $name = $_POST['name'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];

    // Menambahkan data ke database
    $query = "INSERT INTO datauser (name, email, mobile) VALUES (:name, :email, :mobile)";
    $stmt = $dbh->prepare($query);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':mobile', $mobile);

    try {
        $stmt->execute();
        echo "Data berhasil ditambahkan ke database.";
    } catch (PDOException $e) {
        die("Error: " . $e->getMessage());
    }
}
